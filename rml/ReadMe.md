Hi Simo,

two files, with and without the second mirror, both with Si111 monochromator. I don't have the files for the multilayer monochromator, but we use Si111 for everything spectroscopy and diffraction. We normally use "no M2" configuration for most experiments. I didn't install ray on my computer yet, so I didn't test it, but I think it is a rather recent version. I use a 7Tesla bending magnet as the source here, if you have a better idea (1 pole wiggler?) you're welcome to change it. There is no wavelength shifter as a source option in ray.

Best
Ivo
