import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# path to xrt:
import os, sys; sys.path.append(os.path.join('..', '..', '..'))  # analysis:ignore
import xrt.backends.raycing.materials as rm

# raypyng 
from raypyng.postprocessing import PostProcessAnalyzed

# from helper library
from helper_lib import get_reflectivity
from helper_lib import scale_undulator_flux, order

# import simulation parameters
from parameters import energy

# file/folder/ml index definition
flux_simulation_folder = 'RAYPy_Simulation_mySpot_Si111_noM2_Flux'
rp_simulation_folder = 'RAYPy_Simulation_mySpot_Si111_noM2_RP'

ppa = PostProcessAnalyzed()


# Gratings/Source Flux DetectorAtFocus
folder_name  = flux_simulation_folder
source       = 'Dipole'
oe           = 'DetectorAtFocus'
nsim         = energy.shape[0]

from flux import retrieve_flux_beamline
flux_abs, flux_percent, flux_dipole = ppa.retrieve_flux_beamline(folder_name, source, oe, nsim, rounds=1, current=0.3)


### RP DetectorAtFocus
energy_ml_rp = energy
rounds      = 2
folder_name = rp_simulation_folder
oe          = 'DetectorAtFocus'
nsim        = energy_ml_rp.shape[0]

bw, focx, focy = ppa.retrieve_bw_and_focusSize(folder_name, oe, nsim, rounds=rounds)
focx = focx*1000 # in um
focy = focy*1000 # in um


# Gratings/Source Flux DetectorBeforePinhole
folder_name  = flux_simulation_folder
source       = 'Dipole'
oe           = 'DetectorBeforePinhole'
nsim         = energy.shape[0]

from flux import retrieve_flux_beamline
bflux_abs, bflux_percent, bflux_dipole = ppa.retrieve_flux_beamline(folder_name, source, oe, nsim, rounds=1, current=0.3)


### RP DetectorBeforePinhole
energy_ml_rp = energy
rounds      = 2
folder_name = rp_simulation_folder
oe          = 'DetectorBeforePinhole'
nsim        = energy_ml_rp.shape[0]

bbw, bfocx, bfocy = ppa.retrieve_bw_and_focusSize(folder_name, oe, nsim, rounds=rounds)
bfocx = bfocx*1000 # in um
bfocy = bfocy*1000 # in um





########################################
# plotting Flux and RP

fig, (axs) = plt.subplots(4, 2,figsize=(10,10))




# MIRROR COATING
table = 'Henke'
theta = 0.3
E = np.arange(energy[0], energy[-1], 1)
Pt  = rm.Material('Pt',  rho=21.45, kind='mirror',table=table)
Rh  = rm.Material('Rh',  rho=12.45,  kind='mirror',table=table)
PtRh = rm.Multilayer( tLayer=Rh, tThickness=5, 
                        bLayer=Pt, bThickness=60, 
                        nPairs=1, substrate=Pt)

PtRh, _ = get_reflectivity(PtRh, E=E, theta=theta)

ax2=axs[0,0]
ax2.set_xlabel('Energy [eV]')
ax2.set_ylabel('Reflectivity [a.u.]')
ax2.set_title('Mirror Coating Reflectivity')
ax2.plot(E, PtRh, 'black', label='PtRh')
ax2.legend()

# DIPOLE FLUX
ax = axs[0,1]
ax.set_title('Dipole Flux')
ax.grid(which='both', axis='both')
ax.plot(energy, flux_dipole, 'orange', label='Dipole Flux')
ax.set_ylabel('Flux [ph/s/0.1%bw]')

ax.legend()

# BANDWIDTH
ax = axs[1,0]
ss = energy_ml_rp.shape[0]
ax.plot(energy_ml_rp,bbw,'g')


ax.set_xlabel('Energy [eV]')
ax.set_ylabel('Transmitted Bandwidth [eV]')
ax.set_title('Transmitted bandwidth (tbw)')
ax.grid(which='both', axis='both')
# ax.legend()


# RESOLVING POWER
ax = axs[1,1]
ss = energy_ml_rp.shape[0]
ax.plot(energy_ml_rp,energy_ml_rp/bbw,'darkgreen')


ax.set_xlabel('Energy [eV]')
ax.set_ylabel('RP [a.u.]')
ax.set_title('Resolving Power')
ax.grid(which='both', axis='both')
#ax.legend()
# 
# # BEAMLINE TRANSMISSION, NO PINHOLE
ax = axs[2,0]
ax.plot(energy,bflux_percent, 'b', label = 'Si 111, before Pinhole')

ax.set_xlabel(r'Energy [eV]')
ax.set_ylabel('Transmission [%]')
ax.set_title('Available Flux (in transmitted bandwidth)')
ax.grid(which='both', axis='both')
ax.legend()


ax2 = ax.twinx()

ax2.plot(energy,bflux_abs, 'r')
ax2.set_xlabel(r'Energy [eV]')
ax2.set_ylabel('Input Flux [ph/s/tbw]')
ax2.lines.pop(0)
ax2.set_ylabel('Flux [ph/s/tbw]')

# BEAMLINE TRANSMISSION, PINHOLE
ax = axs[2,1]
ax.plot(energy,flux_percent, 'r', label = 'Si 111, after Pinhole')

ax.set_xlabel(r'Energy [eV]')
ax.set_ylabel('Transmission [%]')
ax.set_title('Available Flux (in transmitted bandwidth)')
ax.grid(which='both', axis='both')
ax.legend()


ax2 = ax.twinx()

ax2.plot(energy,flux_abs, 'r')
ax2.set_xlabel(r'Energy [eV]')
ax2.set_ylabel('Input Flux [ph/s/tbw]')
ax2.lines.pop(0)
ax2.set_ylabel('Flux [ph/s/tbw]')



# HORIZONTAL FOCUS
ax = axs[3,0]
afocx = int(np.average(focx))
abfocx = int(np.average(bfocx))
ax.plot(energy_ml_rp, bfocx, 'b', label=fr'{abfocx}$\mu$m')
ax.plot(energy_ml_rp, focx, 'r', label=fr'{afocx}$\mu$m')


ax.set_xlabel('Energy [eV]')
ax.set_ylabel(r'Focus Size [$\mu$m]')
ax.set_title('Horizontal focus')
ax.legend()

# VERTICAL FOCUS
ax = axs[3,1]
afocy = int(np.average(focy))
abfocy = int(np.average(bfocy))
ax.plot(energy_ml_rp, bfocy, 'b', label=fr'{abfocy}$\mu$m')
ax.plot(energy_ml_rp, focy, 'r', label=fr'{afocy}$\mu$m')


ax.set_xlabel('Energy [eV]')
ax.set_ylabel(r'Focus Size [$\mu$m]')
ax.set_title('Vertical focus')
ax.legend()

plt.tight_layout()
plt.savefig('plot/MySpot_Si111_noM2_FluxRpFocus.png')

# plt.show()