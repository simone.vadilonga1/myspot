from raypyng import Simulate
import numpy as np
import os

# import simulation parameters
from parameters import energy, nrays_rp as nrays

this_file_dir=os.path.dirname(os.path.realpath(__file__))
rml_file_name = 'mySpot_Si111_noM2'
rml_file = os.path.join('rml/'+rml_file_name+'.rml')

sim = Simulate(rml_file, hide=True)

rml=sim.rml
beamline = sim.rml.beamline

# cpu
ncpu=5

# rounds
rounds = 2

# name for simulation folder
sim_name = 'mySpot_Si111_noM2_RP'


# define a list of dictionaries with the parameters to scan
params = [      
            {beamline.Dipole.photonEnergy:energy},

            {beamline.Dipole.numberRays:nrays}
        ]
#and then plug them into the Simulation class
sim.params=params

# sim.simulation_folder = '/home/simone/Documents/RAYPYNG/raypyng/test'
sim.simulation_name = sim_name

# turn off reflectivity
sim.reflectivity(reflectivity=False)

# repeat the simulations as many time as needed
sim.repeat = rounds

sim.analyze = True # let RAY-UI analyze the results
## This must be a list of dictionaries
sim.exports  =  [
    {beamline.DetectorAtFocus:['ScalarBeamProperties']},
    {beamline.DetectorBeforePinhole:['ScalarBeamProperties']}
    ]


# create the rml files
#sim.rml_list()

#uncomment to run the simulations
sim.run(multiprocessing=ncpu, force=False)
